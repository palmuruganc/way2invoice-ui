import axios from 'axios';

export const chartService = {
    getStatistics
}

async function getStatistics() {
    let data = {
        labels: [],
        datasets: []
    }

    const purchaseResponse = await axios.get('/api/dashboard/statistics?invoiceType=Purchase&mode=month');
    const salesResponse = await axios.get('/api/dashboard/statistics?invoiceType=Sales&mode=month');
    const profit = await axios.get('/api/dashboard/statistics?mode=month');
    data.labels = prepareLabels(purchaseResponse.data);

    /** Purchase data Preparation */
    let purchaseDatSet = prepareDatasets(purchaseResponse.data, 'Purchase', 'blue', 'blue');
    data.datasets.push(purchaseDatSet);

    /** Sales data preparation */
    let salesDatSet = prepareDatasets(salesResponse.data, 'Sales', 'green', 'green');
    data.datasets.push(salesDatSet);

    /** Profit data preparation */
    let profitDatSet = prepareDatasets(profit.data, 'Profit', 'red', 'red');
    data.datasets.push(profitDatSet);

    return data;
}

function prepareDatasets(responseData, label, backgroundColor, borderColor) {
    var dataSet = {
        label: label,
        fill: false,
        backgroundColor: backgroundColor,
        borderColor: borderColor,
        data: []
    }
    responseData.forEach(data => {
        dataSet.data.push(data.value);
    });

    return dataSet;
}

function prepareLabels(responseData) {
    let labels = [];
    responseData.forEach(data => {
        labels.push(data.key);
    });
    return labels;
}

