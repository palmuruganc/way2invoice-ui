import React from 'react';

import { DropDownBasic } from '../../../components';

const LovBasic = (props) => {
    const selectComponent = [];
    if (props.formData.isChildForm && props.formData.isChildForm === true) {
        selectComponent.push(
            <DropDownBasic name={props.formData.parentForm + "." + props.formData.name} label={props.formData.label} value={props.state[props.formData.parentForm][props.formData.name] || ''} onChangeHandler={props.onChangeHandler}
                data={props.data} hasError={props.hasError} errors={props.errors} state={props.state} code={'id'} description={'name'} />
        );
    } else {
        selectComponent.push(
            <DropDownBasic name={props.formData.name} label={props.formData.label} value={props.state[props.formData.name] || ''} onChangeHandler={props.onChangeHandler}
                data={props.data} hasError={props.hasError} errors={props.errors} state={props.state} code={'id'} description={'name'} />
        );
    }
    return (
        <React.Fragment>
            {selectComponent}
        </React.Fragment>
    );
}

export default LovBasic;