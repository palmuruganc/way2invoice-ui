import React, { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/styles';
import { Grid } from '@material-ui/core';

import { globalService } from '../../service/global-service';

import {
  TotalSales,
  TotalPurchase,
  TotalProfit,
  LatestSales,
  TotalExpense
} from './components';

const useStyles = makeStyles(theme => ({
  root: {
    padding: 8
  }
}));

const Dashboard = () => {
  const classes = useStyles();

  const [totalSales, setTotalSales] = useState();
  const [totalPurchase, setTotalPurchase] = useState();
  const [totalProfit, setTotalProfit] = useState();

  useEffect(() => {
    loadTotalSales();
    loadTotalPurchase();
    loadTotalProfit();
  }, []);

  const loadTotalSales = () => {
    globalService.get('api/dashboard/total?invoiceType=Sales').then(result => {
      setTotalSales(formatCurrency(result.data.value));
    });
  }

  const loadTotalPurchase = () => {
    globalService.get('api/dashboard/total?invoiceType=Purchase').then(result => {
      setTotalPurchase(formatCurrency(result.data.value));
    });
  }

  const loadTotalProfit = () => {
    globalService.get('api/dashboard/profit').then(result => {
      setTotalProfit(formatCurrency(result.data.value));
    });
  }

  const formatCurrency = (amount) => {
    return (new Intl.NumberFormat('en-US', {
      style: 'currency',
      currency: 'INR',
      minimumFractionDigits: 2,
      maximumFractionDigits: 2
    }).format(amount))
  }

  return (
    <div className={classes.root}>
      <Grid container spacing={1}>
        <Grid item lg={3} sm={6} xl={3} xs={12}>
          <TotalSales value={totalSales} />
        </Grid>
        <Grid item lg={3} sm={6} xl={3} xs={12}>
          <TotalPurchase value={totalPurchase} />
        </Grid>
        <Grid item lg={3} sm={6} xl={3} xs={12}>
          <TotalExpense />
        </Grid>
        <Grid item lg={3} sm={6} xl={3} xs={12}>
          <TotalProfit value={totalProfit} />
        </Grid>
        <Grid item lg={12} md={12} xl={9} xs={12}>
          <LatestSales />
        </Grid>

        {
          /**
           * <Grid item lg={6} md={6} xl={3} xs={12}>
          <LatestSales />
        </Grid><UsersByDevice />
           <Grid item lg={4} md={6} xl={3} xs={12}>
                    <LatestProducts />
                  </Grid>
                  <Grid item lg={8} md={12} xl={9} xs={12}>
                    <LatestOrders />
                  </Grid>
           */
        }
      </Grid>
    </div>
  );
};

export default Dashboard;
